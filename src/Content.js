import React, { Fragment } from 'react';
import './Component.scss'

export default class Content extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      content: [
        '1. Warum kann ich nicht direkt online buchen?',
        '2. Zahle ich mehr als bei einer Direktbuchung beim Hotel?',
        '3. Welche Vorteile bietet die Buchung über reisetopia Hotels?',
        '4. Warum bekomme ich bei reisetopia Hotels zusätzliche Vorteile ohne Aufpreis?',
        '6. Wo bezahle ich eine Buchung über reisetopia Hotels?',
        '5. Kann ich eine Buchung über reisetopia Hotels kostenfrei stornieren oder umbuchen?',
        '6. Wo bezahle ich eine Buchung über reisetopia Hotels?',
        '7. Kann ich bei einer Buchung von reisetopia Hotels Loyalitätsprogramme nutzen?'
      ]
    }
  }
  render() {
    return (
      <div className="content">
        {this.state.content.map(el =>
          <div className="content-section">
            <div className="content-section-txt">{el}</div>
            <div className="content-section-arrow" />
          </div>
        )}
      </div>
    )
  }
}