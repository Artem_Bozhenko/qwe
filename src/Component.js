import React from 'react';
import './Component.scss'
import Content from './Content'

export default class Component extends React.Component {
  render() {
    return (
      <div className="wrapper">
        <div className="wrapper-image-section">
          <div className="wrapper-image-section-text">Häufig gestellte Fragen</div>
          <div className="wrapper-image-section-icon" />
        </div>
          <Content />
      </div>
    )
  }
}